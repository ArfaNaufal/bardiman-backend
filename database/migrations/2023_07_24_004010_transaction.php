<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->string('transac_code')->unique();
            $table->unsignedBigInteger('barber_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('menu_id');
            $table->unsignedBigInteger('schedule_id');
            $table->unsignedBigInteger('recommendation_id');
            $table->string('date');
            $table->string('price');
            $table->string('status');
            $table->date('tanggal')->default(Carbon::now());
        
            $table->foreign('schedule_id')->references('id')->on('schedules');
            $table->foreign('barber_id')->references('id')->on('barbers');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('menu_id')->references('id')->on('menus');
            $table->foreign('recommendation_id')->references('id')->on('recommendations');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('transactions');
    }
};
