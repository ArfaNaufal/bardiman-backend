<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Menu;
use App\Models\User;
use App\Models\Category;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        //Category Seeder
        Category::create([
            'name' => 'Hair Cut',
            'image' => 'assets/icons/haircut.png'
        ]);
        
        Category::create([
            'name' => 'Creambath',
            'image' => 'assets/icons/creambath.png'
        ]);
        
        Category::create([
            'name' => 'Hair Colors',
            'image' => 'assets/icons/haircolors.png'
        ]);
        
        Category::create([
            'name' => 'Hair styling',
            'image' => 'assets/icons/hairstyling.png'
        ]);

        Category::create([
            'name' => 'Ear Care',
            'image' => 'assets/icons/cottonbud.png'
        ]);

        Category::create([
            'name' => 'Beard Care',
            'image' => 'assets/icons/beardcare.png'
        ]);

        Category::create([
            'name' => 'Hair Shop',
            'image' => 'assets/icons/shop.png'
        ]);

        Category::create([
            'name' => 'Other',
            'image' => 'assets/icons/other.png'
        ]);

        //Menu Seeder
        Menu::create([
            'name' => 'Signatured Cut',
            'description' => 'Hair recommendation, Hair cut, Hair wash, Hot towell, Hair tonic, Serum, Massage',
            'image' => 'cutbasic.png',
            'duration' => '20',
            'category_id' => '1',
            'Price' => '35000',
        ]);
        Menu::create([
            'name' => 'Creambath',
            'description' => 'Hair treatment using special hair cream product + head massage',
            'image' => 'creambath.png',
            'duration' => '60',
            'category_id' => '2',
            'Price' => '40000',
        ]);
        Menu::create([
            'name' => 'Basic Color Short',
            'description' => 'Colouring paste (dry), bleaching, developer',
            'image' => 'colorshort.png',
            'duration' => '30',
            'category_id' => '3',
            'Price' => '60000',
        ]);
        Menu::create([
            'name' => 'Perming Short',
            'description' => 'Hair wash, tonic, hairstyling plus product styling',
            'image' => 'permingshort.png',
            'duration' => '60',
            'category_id' => '4',
            'Price' => '150000',
        ]);
        Menu::create([
            'name' => 'Ear Candle',
            'description' => 'Ear Cleaning with Ear Candle',
            'image' => 'Earcandle.png',
            'duration' => '30',
            'category_id' => '5',
            'Price' => '60000',
        ]);
        Menu::create([
            'name' => 'Foam Shaving',
            'description' => 'Beard shaving services with Foam',
            'image' => 'Formshaving.png',
            'duration' => '60',
            'category_id' => '6',
            'Price' => '20000',
        ]);
        Menu::create([
            'name' => 'Hair Powder',
            'description' => 'Hair Vitamin',
            'image' => 'powder.png',
            'duration' => '0',
            'category_id' => '7',
            'Price' => '100000',
        ]);
        Menu::create([
            'name' => 'Black Masker',
            'description' => 'Face treatment with black masker, hot towell, and massage',
            'image' => 'blck.png',
            'duration' => '30',
            'category_id' => '8',
            'Price' => '20000',
        ]);
        User::create([
            'username' => 'Admin',
            'email' => 'arfaazizan.naufal@gmail.com',
            'password' => bcrypt('12345678'),
            'role' => '1',
        ]);
    }
}
