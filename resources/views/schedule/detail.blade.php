@extends('dashboard.layouts.main')

@section('contain')
<div class="topdetail">
    <center><h1 >Halaman Detail Schedule</h1></center>
</div>
{{-- create table with data from array siswa --}}

<div class="form-group p-3">
    <label for="">Barber Id</label>
    <input type="text" class="form-control" name="barber_id" id="barber_id" value="{{ $s->username }} " readonly>
</div>
<div class="form-group p-3">
    <label for="">Hour</label>
    
@foreach ( $s->schedule as $sdata)
    <div style="display: flex; flex-direction:row; justify-content:space-between; margin-bottom:20px;">
        <input type="text" class="form-control" style="height: 50px; margin-right:20px;" name="hour" id="hour" value="{{ $sdata->hour }} " readonly>
        <button onclick="location.href=`{{url('dashboard/schedule/edit/'.$sdata->id)}}`" class="btn-action" style="height: 50px; background-color: var(--primary-color); color: white;"><i class="uil uil-edit-alt"></i></button>
    </div>
@endforeach

</div>

<div class="form-group p-3">
    <a href="/dashboard/schedule/all" class="btn btn-outline-primary">Go Back</a>
</div>


@endsection