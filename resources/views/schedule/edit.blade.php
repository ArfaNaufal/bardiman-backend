@extends('dashboard.layouts.main')

@section('contain')
<div>
    <div class="topdetail">
    <center><h1 >Halaman Edit Schedule</h1></center>
</div>
    <div class="col-lg-8">
        <form method="post" action="/dashboard/schedule/update/{{ $schedule->id }}" enctype="multipart/form-data">
            @csrf
            <div class="mb-3 mt-3 ">
                <label for="hour" class="from-label">Hour</label>
                <input type="text" class="form-control" id="hour" name="hour"
                    value="{{ old('username', $schedule->hour) }}" disable>
            </div>
            <button type="submit" class="btn btn-primary">Ubah Data</button>
        </form>
    </div>

    <script>
        var dropzone = document.getElementById('dropzone');
        var fileInput = document.getElementById('file-input');
        var browseBtn = document.getElementById('browse-btn');
    dropzone.addEventListener('dragenter', (e) => {
            e.preventDefault();
            dropzone.classList.add('dragover');
        });
    
        dropzone.addEventListener('dragover', (e) => {
            e.preventDefault();
          
        });
    
        dropzone.addEventListener('dragleave', () => {
            dropzone.classList.remove('dragover');
         
            });
    dropzone.addEventListener('drop', (e) => {
            e.preventDefault();
            dropzone.classList.remove('dragover');
            fileInput.files = e.dataTransfer.files;
            console.log(e.dataTransfer.files);
            });
    browseBtn.addEventListener('click', () => {
            fileInput.click();
            });
    
    </script>
    @endsection
    
    <script>
        var loadFile = function(event) {
          var reader = new FileReader();
          reader.onload = function(){
            var output = document.getElementById('output');
            var oldimage = document.getElementById('oldimage');
            oldimage.style.display = 'none';
            output.style.display = 'block'; 
            output.src = reader.result;
          };
          reader.readAsDataURL(event.target.files[0]);
        };
      </script>