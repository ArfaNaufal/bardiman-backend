<nav class="navbar navbar-expand-lg bg-light">
    <div class="container-fluid">
      <a class="navbar-brand" href="#">BARDIMAN</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            
        </ul>
        {{-- <form class="d-flex" role="search">
          <a class="btn btn-outline-success me-2" href="/login/all">Sign in</a>
          <a class="btn btn-outline-danger" href="/register/all">Sign up</a>
        </form> --}}
        <ul class="navbar-nav ms-auto">
          @auth
          <li>             
              <li>
                <form action="/session/logout" method="post">
                  @csrf
                  <button type="submit" class="dropdown-item">
                    <i class="bi bi-box-arrow-right">Logout</i>
                  </button>
                </form>
              </li>
          </li>
          @else
            <li class="nav-item">
              <a class="nav-link" href="/session/login">Dashboard</a>
            </li>
          @endauth
        </ul>
      </div>
    </div>
  </nav>