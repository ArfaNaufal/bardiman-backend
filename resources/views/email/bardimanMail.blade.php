<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bardiman</title>
</head>

<body>

    <h1>{{ $mailData['title'] }}</h1>

    <p>{{ $mailData['body'] }}</p>

    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Veniam magni quidem dolorum tenetur laudantium
        asperiores accusamus officia enim itaque, quam tempore adipisci unde quasi assumenda consequatur, perferendis
        dolore fugiat. Similique!</p>
    <p>thanks!</p>

</body>

</html>
