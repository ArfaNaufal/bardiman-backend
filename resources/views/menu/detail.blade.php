@extends('dashboard.layouts.main')

@section('contain')
<div class="topdetail">
    <center><h1 >Halaman Detail Menu</h1></center>
</div>
{{-- create table with data from array siswa --}}

<div class="form-group p-3">
    <label for="">Name</label>
    <input type="text" class="form-control" name="name" id="name" value="{{ $s->name }} " readonly>
</div>

<div class="form-group p-3">
    <label for="" >Description</label>
    <input type="text" class="form-control" name="description" id="description" value="{{ $s->description }}" readonly>
</div>

<div class="form-group p-3">
    <label for="" >Image</label><br>
    <img src="/images/menu/{{ $s->image }}" width="100"  class="img img-responsive">
</div>

<div class="form-group p-3">
    <label for="" >category</label>
    <input type="text" class="form-control" name="category" id="category" value="{{ $s->category->name }}" readonly>
</div>

<div class="form-group p-3">
    <label for="" >price</label>
    <input type="text" class="form-control" name="price" id="price" value="{{'Rp'.number_format($s->Price,0,',','.')}}" readonly>
</div>

<div class="form-group p-3">
   
    <a href="/dashboard/menu/all" class="btn btn-outline-primary">Go Back</a>
</div>
 
@endsection