@extends('dashboard.layouts.main')

@section('contain')
<h1 align="center">Edit Data Menu</h1>
<div class="col-lg-8">
    <form method="post" action="/dashboard/menu/update/{{$menu->id}}" enctype="multipart/form-data">
    @csrf
    <div class="mb-3">
        <label for="name" class="from-label">Name</label>
        <input type="text" class="form-control" id="name" name="name" value="{{old('name', $menu->name)}}" disable>
    </div>
    <div class="mb-3">
        <label for="description" class="from-label">description</label>
        <input type="text" class="form-control" id="description" name="description" value="{{old('description', $menu->description)}}" disable>
    </div>
    <label for="">Image</label>
    <div id="dropzone" class="dropzone">
        <div class="drop-message mb-4">
            Drag and drop files here or click to upload or replace file.
        </div>
        <input type="file" class="form-control" name="image" id="file-input" aria-describedby="inputGroupFileAddon04" aria-label="Upload"accept="image/*" onchange="loadFile(event)" style="display:none;">
        <button type="button" id="browse-btn">Browse</button>
          </div>
    <img id="output"width="100px" height="100px" class="img img-responsive"style='display:none;'/>
    @if ($menu->image !== null)
    <img id="oldimage" src="/images/menu/{{ $menu->image }}" width="100px" height="100px" class="img img-responsive">
   
    @endif
    <div class="mb-3">
        <label for="duration" class="from-label">duration</label>
        <input type="text" class="form-control" id="duration" name="duration" value="{{old('duration', $menu->duration)}}" disable>
    </div>
    <div class="mb-3">
        <label for="category_id" class="from-label">Category</label>
        <select class="form-select" name="category_id" id="">
            @foreach ($category as $class)
                @if (old('category_id', $menu->category_id == $class->id))
                    <option name="category_id" value="{{ $class->id }}" selected>{{ $class->name }}</option>
                @else
                    <option name="category_id" value="{{ $class->id }}">{{ $class->name }}</option>
                @endif

            @endforeach
        </select>
    </div>
    <div class="mb-3">
        <label for="Price" class="from-label">Price</label>
        <input type="text" class="form-control" id="Price" name="Price" value="{{old('Price', $menu->Price)}}">
    </div>
    
    <button type="submit" class="btn btn-primary">Ubah Data</button>
</form>
</div>
<script>
    var dropzone = document.getElementById('dropzone');
    var fileInput = document.getElementById('file-input');
    var browseBtn = document.getElementById('browse-btn');
dropzone.addEventListener('dragenter', (e) => {
        e.preventDefault();
        dropzone.classList.add('dragover');
    });

    dropzone.addEventListener('dragover', (e) => {
        e.preventDefault();
      
    });

    dropzone.addEventListener('dragleave', () => {
        dropzone.classList.remove('dragover');
     
        });
dropzone.addEventListener('drop', (e) => {
        e.preventDefault();
        dropzone.classList.remove('dragover');
        fileInput.files = e.dataTransfer.files;
        console.log(e.dataTransfer.files);
        });
browseBtn.addEventListener('click', () => {
        fileInput.click();
        });

</script>
@endsection

<script>
    var loadFile = function(event) {
      var reader = new FileReader();
      reader.onload = function(){
        var output = document.getElementById('output');
        var oldimage = document.getElementById('oldimage');
        oldimage.style.display = 'none';
        output.style.display = 'block'; 
        output.src = reader.result;
      };
      reader.readAsDataURL(event.target.files[0]);
    };
  </script>