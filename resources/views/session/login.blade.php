@extends('layouts.main')

@section('container')
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="/css/login.css">
        <title>Login</title>
    </head>
    <body>
        <div class="container">
        <div class="box form-box">
            <form method="post" action="/session/logins">
                @csrf
                <header>Login</header>
                <div class="mt-5">
                        @if($errors->any())
                            <div class="col-12">
                                @foreach ($errors->all() as $loginError)
                                    <div class="alert alert-danger">{{$loginError}}</div>
                                     {{-- <div class="alert alert-danger">{{'Login Gagal'}}</div>  --}}
                                @endforeach
                            </div>
                        @endif


                        @if (session()->has('success'))
                            <div class="alert alert-success">{{$success}}</div>
                        @endif
                </div> 
                <div class="form-floating mb-3">
                    <input type="email" class="form-control" id="floatingInput" name="email" placeholder="name@example.com">
                    <label for="floatingInput">Email address</label>
                  </div>
                  <div class="form-floating">
                    <input type="password" class="form-control" id="floatingPassword" name="password" placeholder="Password">
                    <label for="floatingPassword">Password</label>
                  </div>
                <div class="field">
                    <input class="login" type="submit" name="submit" id="floatingLogin" value="Login" required>
                </div>
                </form>
            </div>
        </div>
    </body>
</html>
    
@endsection
