@extends('layouts.main')

@section('container')
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="/css/login.css">
        <title>Login</title>
    </head>
    <body>
        <div class="container">
        <div class="box form-box">
            <div class="mt-5">
                @if($errors->any())
                    <div class="col-12">
                        @foreach ($errors->all() as $loginError)
                            <div class="alert alert-danger">{{$loginError}}</div>
                             {{-- <div class="alert alert-danger">{{'Login Gagal'}}</div>  --}}
                        @endforeach
                    </div>
                @endif


                @if (session()->has('success'))
                    <div class="alert alert-success">{{$success}}</div>
                @endif
            </div>
            <form method="post" action="{{ route('password.update')}}">
                @csrf
                <header>Reset your password</header>
                <input type="hidden" name="token" value="{{request()->token}}">
                <input type="hidden" name="email" value="{{request()->email}}">
                <div class="form-floating mb-3">
                    <input type="password" class="form-control" id="floatingInput" name="password" placeholder="password">
                    <label for="floatingInput">password</label>
                  </div>
                  <div class="form-floating mb-3">
                    <input type="password" class="form-control" id="floatingInput" name="password_confirmation" placeholder="password">
                    <label for="floatingInput">confirm password</label>
                  </div>
                <div class="field">
                    <input class="login" type="submit" name="submit" id="floatingLogin" value="Confirm" required>
                </div>
                </form>
            </div>
        </div>
    </body>
</html>

@endsection
