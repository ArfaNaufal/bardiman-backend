@extends('dashboard.layouts.main')

@section('contain')
<center><h1>Halaman Detail Porto</h1></center>
{{-- create table with data from array siswa --}}

<div class="form-group p-3">
    <label for="">Id</label>
    <input type="text" class="form-control" name="username" id="username" value="{{ $s->id }} " readonly>
</div>

<div class="form-group p-3">
    <label for="">Description</label>
    <input type="text" class="form-control" name="description" id="description" value="{{ $s->description }} " readonly>
</div>

<div class="form-group p-3">
    <label for="" >Image</label>
    <input type="text" class="form-control" name="image" id="image" value="{{ $s->image }}" readonly>
</div>

<div class="form-group p-3">
    <label for="" >Barber Id</label>
    <input type="text" class="form-control" name="barber_id" id="barber_id" value="{{ $s->barber_id }}" readonly>
</div>

<a href="/dashboard/barber/all" class="btn btn-outline-primary">Go Back</a>
@endsection