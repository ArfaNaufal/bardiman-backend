@extends('dashboard.layouts.main')

@section('contain')
<div>
    <div class="topdetail">
    <center><h1 >Halaman Edit Portofolio</h1></center>
</div>
    <div class="col-lg-8">
        <form method="post" action="/dashboard/porto/update/{{ $porto->id }}" enctype="multipart/form-data">
            @csrf

            <div class="mb-3 mt-3 ">
                <label for="description" class="from-label">Barber Description</label>
                <input type="text" class="form-control" id="description" name="description"
                    value="{{ old('username', $porto->description) }}" disable>
            </div>

        
        <label for="">Image</label>
        <div id="dropzone" class="dropzone">
            <div class="drop-message mb-4">
                Drag and drop files here or click to upload or replace file.
            </div>
            <input type="file" class="form-control" name="image" id="file-input" aria-describedby="inputGroupFileAddon04" aria-label="Upload"accept="image/*" onchange="loadFile(event)" style="display:none;">
            <button type="button" id="browse-btn">Browse</button>
              </div>
        <img id="output"width="100px"  class="img img-responsive"style='display:none;'/>
        @if ($porto->image !== null)
        <img id="oldimage" src="/images/porto/{{ $porto->image }}" width="100px" class="img img-responsive mt-3 mb-3" >
        @endif            
            <br>

            <button type="submit" class="btn btn-primary">Ubah Data</button>
        </form>
    </div>

    <script>
        var dropzone = document.getElementById('dropzone');
        var fileInput = document.getElementById('file-input');
        var browseBtn = document.getElementById('browse-btn');
    dropzone.addEventListener('dragenter', (e) => {
            e.preventDefault();
            dropzone.classList.add('dragover');
        });
    
        dropzone.addEventListener('dragover', (e) => {
            e.preventDefault();
          
        });
    
        dropzone.addEventListener('dragleave', () => {
            dropzone.classList.remove('dragover');
         
            });
    dropzone.addEventListener('drop', (e) => {
            e.preventDefault();
            dropzone.classList.remove('dragover');
            fileInput.files = e.dataTransfer.files;
            console.log(e.dataTransfer.files);
            });
    browseBtn.addEventListener('click', () => {
            fileInput.click();
            });
    
    </script>
    @endsection
    
    <script>
        var loadFile = function(event) {
          var reader = new FileReader();
          reader.onload = function(){
            var output = document.getElementById('output');
            var oldimage = document.getElementById('oldimage');
            oldimage.style.display = 'none';
            output.style.display = 'block'; 
            output.src = reader.result;
          };
          reader.readAsDataURL(event.target.files[0]);
        };
      </script>