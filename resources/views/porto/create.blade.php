@extends('dashboard.layouts.main')

@section('contain')
    <center style="margin-top:80px; font-family: 'Poppins', sans-serif;">
        <h1>Halaman Create Porto</h1>
    </center>
    {{-- create table with data from array siswa --}}

    <form method="post" action="/dashboard/porto/add/{{$barberid}}" enctype="multipart/form-data">
        @csrf

        <div class="card">
            <div class="card-body">

                <div class="mb-3">
                    <label for="">Barber Description</label>
                    <input type="text" class="form-control" name="description" id="description" placeholder="Insert Barber Description">
                </div>
                <label for="">Image</label>
                <div id="dropzone" class="dropzone">
                    <div class="drop-message mb-4">
                        Drag and drop files here or click to upload or replace file.
                    </div>
                    <input type="file" class="form-control" name="image" id="file-input" aria-describedby="inputGroupFileAddon04" aria-label="Upload"accept="image/*" onchange="loadFile(event)" style="display:none;">
                    <button type="button" id="browse-btn">Browse</button>
                </div>
                <img id="output"width="100px" height="100px" class="img img-responsive"style='display:none;'/>
                {{-- <div class="mb-3 mt-3">
                    <label for="category" class="form-label">Barber Id</label>
                    <select class="form-select" name="barber_id" id="">
                        @foreach ($barber as $class)
                            <option name="barber_id" value="{{ $class->id }}">{{ $class->username }}</option>
                        @endforeach
                    </select>

                </div> --}}
                

            </div>
            <button type="submit" class="btn btn-primary">Send</button>

        

    </form>
    <script>
        var dropzone = document.getElementById('dropzone');
        var fileInput = document.getElementById('file-input');
        var browseBtn = document.getElementById('browse-btn');
    dropzone.addEventListener('dragenter', (e) => {
            e.preventDefault();
            dropzone.classList.add('dragover');
        });

        dropzone.addEventListener('dragover', (e) => {
            e.preventDefault();
          
        });

        dropzone.addEventListener('dragleave', () => {
            dropzone.classList.remove('dragover');
         
        });
    dropzone.addEventListener('drop', (e) => {
            e.preventDefault();
            dropzone.classList.remove('dragover');
            fileInput.files = e.dataTransfer.files;
            console.log(e.dataTransfer.files);
        });
    browseBtn.addEventListener('click', () => {
            fileInput.click();
        });

    </script>
@endsection

<script>
var loadFile = function(event) {
    var reader = new FileReader();
    reader.onload = function(){
        var output = document.getElementById('output');
       
        output.style.display = 'block'; 
        output.src = reader.result;
      };
      reader.readAsDataURL(event.target.files[0]);
    };
  </script>