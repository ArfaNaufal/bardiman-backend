<table>
    <thead>
        <tr>
            <th>Transac Code</th>
            <th>Barber</th>
            <th>User</th>
            <th>Menu</th>
            <th>Date Schedule</th>
            <th>Status</th>
            <th>Order Date</th>
            <th>Price</th>
        </tr>
    </thead>
    <tbody>
        @forelse ( $done as $item )
        <tr>
            <td>{{ $item->transac_code }}</td>
            <td>{{ $item->barber->username}}</td>
            <td>{{ $item->customer->username}}</td>
            <td>{{ $item->menu->name}}</td>
            <td>{{ $item->date.' - '.$item->schedule->hour}}</td>
            <td>{{ $item->status}}</td>
            <td>{{formatDate($item->tanggal)}}</td>
            <td>{{ $item->price}}</td>
        </tr>
        @empty
        <tr>
            <td colspan="10" align="center">Tidak Ada Data</td>
        </tr>
        @endforelse
    </tbody>
    <tfoot>
        <tr>
            <th colspan="7">Total Keseluruhan</th>
            <th>{{$total}}</th>
        </tr>
    </tfoot>
</table>

@php
  function formatDate($date)
  {
    $carbonDate = \Carbon\Carbon::createFromFormat('Y-m-d', $date);
    $formattedDate = $carbonDate->format('d-m-Y'); // Format tanggal seperti yang Anda lakukan sebelumnya

    // Mengonversi bulan ke format huruf (misalnya, "Januari" bukan "01")
    $formattedDate = strftime('%d %B %Y', $carbonDate->timestamp); // %B adalah placeholder untuk nama bulan dalam bahasa yang sesuai

    return $formattedDate;
  }
  @endphp