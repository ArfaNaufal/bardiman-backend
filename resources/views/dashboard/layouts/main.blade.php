<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bardiman</title>
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://unicons.iconscout.com/release-pro/v4.0.0/css/solid.css">
    <link rel="stylesheet" href="https://unicons.iconscout.com/release/v4.0.8/css/line.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  </head>
  <body>
    <nav>
      <div class="logo-name">
        <div class="logo-image">
          <img src="/images/logo/bardiman_logo.png">
        </div>

        <span class="logo_name">Bardiman</span>
      </div>
      <div class="menu-items">
        <ul class="nav-links">
          <li>
            <a href="/dashboard/recap/all">
              <i class="uil uil-clipboard-alt"></i>
              <span class="link-name">Recap Transaction</span>
            </a>
          </li>
          <li>
            <a href="/dashboard/transaction/all">
              <i class="uil uil-clipboard-alt"></i>
              <span class="link-name">Transaction</span>
            </a>
          </li>
          
          <li>
            <a href="/dashboard/menu/all">
              <i class="uil uil-estate"></i>
              <span class="link-name">Menu Service</span>
            </a>
          </li>
          <li>
            <a href="/dashboard/barber/all">
              <i class="uil uil-user-circle"></i>
              <span class="link-name">Barber</span>
            </a>
          </li>
          
        </ul>

        <ul class="logout-mod">
          <li>
            <a href="/session/logout" method="POST">
              <i class="uil uil-signout"></i>
              <span class="link-name">Sign Out</span>
            </a>
          </li>
        </ul>
      </div>
    </nav>

    

    <section class="dashboard">
      
      @yield('contain')
      
    </section>

    
    <script src="/js/script.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.3/dist/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

  </body>
</html>
