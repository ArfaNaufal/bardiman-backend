@extends('dashboard.layouts.main')


@section('contain')
<div class="dash-content">
    <div style="display:flex; flex-direction:column;">
        <div class="wrapper">
              <div class="title">
                <i class="uil uil-tachometer-fast-alt"></i>
                <span class="text">Schedule Barber</span>
              </div>
              <form action='all' style="display:flex; margin-top:30px; flex-direction:row; gap:20px; width:600px;">
                <div class="search-box">
                    <input type="text" username='queryvalue' placeholder="Search  here...">
                </div>
                <button style="height:45px; width:45px;border:none;background-color:white;font-size:25wpx;"><i class="uil uil-search"></i></button>
              </form>
             <a href="create" class="btn-tambah">
                <div class="title" >
                    <i class="uil uil-plus"></i>
                    <span class="text">Tambah</span>
                </div> 
             </a>
        </div>
        <table class="table table-striped">
        <thead>
          <tr>
            <th scope="col">No</th>
            <th scope="col">Barber</th>
            <th scope="col">Hour</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
            <?php $index = 1;?>
            @foreach ($data_schedule as $data )
            
          <tr>
            <th scope="row">{{$index++}}</th>
            
            <td>{{$data->username}}</td>
            <td>
              @foreach ( $data->schedule as $schedule )
                {{"-".$schedule->hour}} <br>
              @endforeach
            </td>
            <td style="display:flex; flex-direction:column; gap:20px;">
              <button onclick="location.href='detail/{{$data->id}}'" class="btn-action" style="background-color: var(--box1-color); color: white;"><i class="uil uil-newspaper"></i></button>
            </td>
          </tr>
            @endforeach                
        </tbody> 
    </table>
    <div id="myModal" class="modal fade">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header flex-column">
                    <div class="icon-box">
                        <i class="material-icons">&#xE5CD;</i>
                    </div>						
                    <h4 class="modal-title w-100">Are you sure?</h4>	
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <p>Do you really want to delete <span id="modalId"></span>? This process cannot be undone.  </p>
                </div>
                <div class="modal-footer justify-content-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" id="deletebutton" data-dismiss="modal" class="btn btn-danger">Delete</button>
                </div>
            </div>
        </div>
    </div>     
    <div class="d-flex justify-content-end me-4 mt-4" style="gap:20px; ">
        <!-- a Tag for previous page -->
    <a class="pagination-btn" href="{{$data_schedule->previousPageUrl()}}">
    <!-- You can insert logo or text here -->
    <i class="uil uil-arrow-left"></i>
    </a>
    @for($i=1;$i<=$data_schedule->lastPage();$i++)
        <!-- a Tag for another page -->
        <a class="pagination-btn" href="{{$data_schedule->url($i)}}">{{$i}}</a>
    @endfor
    <!-- a Tag for next page -->
    <a class="pagination-btn" href="{{$data_schedule->nextPageUrl()}}">
        <!-- You can insert logo or text here -->
        <i class="uil uil-arrow-right"></i>
    </a>
    </div>
    </div>
  </div>
@endsection

<script>
    document.addEventListener('DOMContentLoaded', function() {
      var modalTriggers = document.querySelectorAll('[data-toggle="modal"]');
      var modalIdElement = document.getElementById('modalId');
      var btndelete = document.getElementById('deletebutton')
  
      modalTriggers.forEach(function(trigger) {
        trigger.addEventListener('click', function() {
          var id = this.getAttribute('data-id');
          var username = this.getAttribute('data-username');
          modalIdElement.textContent = username;
          btndelete.addEventListener('click', function() {
            window.location.href = '/dashboard/schedule/delete/' +id
          })
        });
      });
    });
  </script>