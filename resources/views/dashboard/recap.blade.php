@extends('dashboard.layouts.main')


@section('contain')
<div class="dash-content">
    
    <div style="display:flex; flex-direction:column;">
        <div class="wrapper">
              <div class="title">
                <i class="uil uil-tachometer-fast-alt"></i>
                <span class="text">Recap Transaction</span>
              </div>
              <div class="titles" style='background-color:#F5C333; padding:10px; border-radius: 10px; '>
                @if ($barbervalue!=null&&$created_atd!=null)
                <span class="text">{{$barbervalue!=null?$barbervalue->username:''}} - {{formatDate($created_atd)}} Total Income = {{'Rp'.number_format($total,0,',','.')}}</span>
                @elseif($created_atd!=null)
                <span class="text">{{formatDate($created_atd)}} Total Income = {{'Rp'.number_format($total,0,',','.')}}</span>
                @elseif ($barbervalue!=null)
                <span class="text">{{$barbervalue!=null?$barbervalue->username:''}} Total Income = {{'Rp'.number_format($total,0,',','.')}}</span>
                @else 
                <span class="text">Total Income = {{'Rp'.number_format($total,0,',','.')}}</span>
                @endif
              </div>
              <form action="/dashboard/recap/all" id="formd">
                <select class='select' name="barber_id" onchange="changed()">
                  <option value="">Select Barber</option>
                  @foreach ($data_barber as $datad )
                  <option value="{{ $datad->id}}" <?php echo $idbarber==$datad->id?'selected':''?>>
                    {{ $datad->username}}
                  </option>
                  @endforeach
                </select>
                {{-- <select class="select" name="create_atreq" onchange="changed()">
                  <option value="">Select</option>
                  @foreach ($data_transactiond as $datadt)
                      <option value="{{ $datadt }}" {{ $created_atd == $datadt ? 'selected' : '' }}>
                          {{ $datadt }}
                      </option>
                  @endforeach
              </select> --}}
              <input class="select" name="create_atreq" type="date" value="{{ $created_atd}}" onchange="changed()">
              </form>
              <form action='all' style="display:flex; margin-top:30px; flex-direction:row; gap:20px; width:300px;">
                <div class="search-box">
                    <input type="text" value="{{ $value_search }}"name='queryvalue' placeholder="Search  here...">
                </div>
                <button style="height:45px; width:45px;border:none;background-color:white;font-size:25wpx;"><i class="uil uil-search"></i></button>
              </form>
        </div>
        <table class="table table-striped">
        <thead>
          <tr>
            <th scope="col">Id</th>
            <th scope="col">Transaction Code</th>
            <th scope="col">Barber</th>
            <th scope="col">Customer</th>
            <th scope="col">Menu</th>
            <th scope="col">Date Schedule</th>
            <th scope="col">Price</th>
            <th scope="col">Status</th>
            <th scope="col">Order Date</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($data_transaction as $data )
            @php
            $index = ($data_transaction->currentPage() - 1) * $data_transaction->perPage() + $loop->iteration;
            @endphp

          <tr>
            <th scope="row">{{$index++}}</th>
            <th>{{$data->transac_code}}</th>
            <td>{{$data->barber->username}}</td>
            <td>{{$data->customer->username}}</td>
            <td>{{$data->menu->name}}</td>
            <td>{{$data->date.' - '.$data->schedule->hour}}</td>
            <td>{{'Rp'.number_format($data->price,0,',','.')}}</td>
            <td style="{{$data->status=='done'?"color:rgb(104, 228, 104)":"color:red"}}">{{$data->status}}</td>
            <td>{{formatDate($data->tanggal)}}</td>
            
            {{-- <td style="display:flex; flex-direction:column; gap:20px;">
              <button onclick="location.href='edit/{{$data->id}}'" class="btn-action" style="background-color: var(--primary-color); color: white;"><i class="uil uil-edit-alt"></i></button>
              <button onclick="location.href='detail/{{$data->id}}'" class="btn-action" style="background-color: var(--box1-color); color: white;"><i class="uil uil-newspaper"></i></button>
              <button type="button" data-id="{{$data->id}}"  data-username="{{$data->username}}" data-toggle="modal" data-target="#myModal" class="btn-action" style="background-color: var(--box2-color); color: white;"><i class="uil uil-trash"></i></button>
            </td> --}}
          </tr>
            @endforeach                
        </tbody> 
    </table>
    <div id="myModal" class="modal fade">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header flex-column">
                    <div class="icon-box">
                        <i class="material-icons">&#xE5CD;</i>
                    </div>						
                    <h4 class="modal-title w-100">Are you sure?</h4>	
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <p>Do you really want to delete <span id="modalId"></span>? This process cannot be undone.  </p>
                </div>
                <div class="modal-footer justify-content-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" id="deletebutton" data-dismiss="modal" class="btn btn-danger">Delete</button>
                </div>
            </div>
        </div>
    </div>     
    <div class="d-flex justify-content-end me-4 mt-4" style="gap:20px; ">
      <!-- a Tag for previous page -->
      @if ($data_transaction->currentPage()>1)
      <a class="pagination-btn" href="?barber_id={{$idbarber}}&create_atreq={{$created_atd}}&page={{$data_transaction->currentPage()-1}}">
        <!-- You can insert logo or text here -->
        <i class="uil uil-arrow-left"></i>
      </a>  
      @endif
      @for($i=1;$i<=$data_transaction->lastPage();$i++)
      <!-- a Tag for another page -->
      <a class="pagination-btn" href="?barber_id={{$idbarber}}&create_atreq={{$created_atd}}&page={{$i}}">{{$i}}</a>
      @endfor
      <!-- a Tag for next page -->
      
      @if ($data_transaction->currentPage() < $data_transaction->lastPage())
      <a class="pagination-btn" href="?barber_id={{$idbarber}}&create_atreq={{$created_atd}}&page={{$data_transaction->currentPage()+1}}">
        <!-- You can insert logo or text here -->
        <i class="uil uil-arrow-right"></i>
      </a>
      @endif
    </div>
    <div class="d-flex justify-content-end me-4 mt-4" style="gap:20px; ">
      <form action='{{url('/dashboard/recap/excel/'.$data_transaction2)}}'>
        <input type="hidden" value='{{$idbarber}}' name="barber_id">
        <input type="hidden" value='{{$created_atd}}' name="create_atreq">
        {{-- <input type="text" value='{{$value_search}}' name="queryvalue"> --}}
        <button type="submit" class="titles" style='background-color:#F5C333; padding:10px; border-radius: 10px; '>export excel</button>
      </form>
    </div>
    </div>
  </div>
@endsection

<script>
    document.addEventListener('DOMContentLoaded', function() {
      var modalTriggers = document.querySelectorAll('[data-toggle="modal"]');
      var modalIdElement = document.getElementById('modalId');
      var btndelete = document.getElementById('deletebutton')
  
      modalTriggers.forEach(function(trigger) {
        trigger.addEventListener('click', function() {
          var id = this.getAttribute('data-id');
          var name = this.getAttribute('data-name');
          modalIdElement.textContent = name;
          btndelete.addEventListener('click', function() {
            window.location.href = '/dashboard/menu/delete/' +id
          })
        });
      });
    });
  
  function changed(){
    const form = document.getElementById('formd')
    form.submit()
  }
  </script>

  @php
  function formatDate($date)
  {
    $carbonDate = \Carbon\Carbon::createFromFormat('Y-m-d', $date);
    $formattedDate = $carbonDate->format('d-m-Y'); // Format tanggal seperti yang Anda lakukan sebelumnya

    // Mengonversi bulan ke format huruf (misalnya, "Januari" bukan "01")
    $formattedDate = strftime('%d %B %Y', $carbonDate->timestamp); // %B adalah placeholder untuk nama bulan dalam bahasa yang sesuai

    return $formattedDate;
  }
  @endphp