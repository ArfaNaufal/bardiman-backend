@extends('dashboard.layouts.main')


@section('contain')
<div class="dash-content">
    <div style="display:flex; flex-direction:column;">
        <div class="wrapper">
              <div class="title">
                <i class="uil uil-tachometer-fast-alt"></i>
                <span class="text">User</span>
              </div>
              <form action='all' style="display:flex; margin-top:30px; flex-direction:row; gap:20px; width:600px;">
                <div class="search-box">
                    <input type="text" name='queryvalue' placeholder="Search  here...">
                </div>
                <button style="height:45px; width:45px;border:none;background-color:white;font-size:25wpx;"><i class="uil uil-search"></i></button>
              </form>
             
        </div>
        <table class="table table-striped">
        <thead>
          <tr>
            <th scope="col">Id</th>
            <th scope="col">UserName</th>
            <th scope="col">Email</th>
            <th scope="col">role</th>
            <th scope="col">Profile</th>
            <th scope="col">Age</th>
            <th scope="col">Adress</th>
            <th scope="col">Phone</th>
          </tr>
        </thead>
        <tbody>
            <?php $index = 1;?>
            @foreach ($data_user as $data )
            
          <tr>
            <th scope="row">{{$index++}}</th>
            <td>{{$data->username}}</td>
            <td>{{$data->email}}</td>
            <td>{{$data->role}}</td>
            <td>{{$data->profile_photo_path}}</td>
            <td>{{$data->age}}</td>
            <td>{{$data->adress}}</td>
            <td>{{$data->phone_number}}</td>
            
          </tr>
            @endforeach                
        </tbody> 
    </table>
    <div id="myModal" class="modal fade">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header flex-column">
                    <div class="icon-box">
                        <i class="material-icons">&#xE5CD;</i>
                    </div>						
                    <h4 class="modal-title w-100">Are you sure?</h4>	
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <p>Do you really want to delete <span id="modalId"></span>? This process cannot be undone.  </p>
                </div>
                <div class="modal-footer justify-content-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" id="deletebutton" data-dismiss="modal" class="btn btn-danger">Delete</button>
                </div>
            </div>
        </div>
    </div>     
    <div class="d-flex justify-content-end me-4 mt-4" style="gap:20px; ">
        <!-- a Tag for previous page -->
    <a class="pagination-btn" href="{{$data_user->previousPageUrl()}}">
    <!-- You can insert logo or text here -->
    <i class="uil uil-arrow-left"></i>
    </a>
    @for($i=1;$i<=$data_user->lastPage();$i++)
        <!-- a Tag for another page -->
        <a class="pagination-btn" href="{{$data_user->url($i)}}">{{$i}}</a>
    @endfor
    <!-- a Tag for next page -->
    <a class="pagination-btn" href="{{$data_user->nextPageUrl()}}">
        <!-- You can insert logo or text here -->
        <i class="uil uil-arrow-right"></i>
    </a>
    </div>
    </div>
  </div>
@endsection

<script>
    document.addEventListener('DOMContentLoaded', function() {
      var modalTriggers = document.querySelectorAll('[data-toggle="modal"]');
      var modalIdElement = document.getElementById('modalId');
      var btndelete = document.getElementById('deletebutton')
  
      modalTriggers.forEach(function(trigger) {
        trigger.addEventListener('click', function() {
          var id = this.getAttribute('data-id');
          var name = this.getAttribute('data-name');
          modalIdElement.textContent = name;
          btndelete.addEventListener('click', function() {
            window.location.href = '/dashboard/menu/delete/' +id
          })
        });
      });
    });
  </script>