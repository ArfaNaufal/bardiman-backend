@extends('dashboard.layouts.main')

@section('contain')
    <center style="margin-top:80px; font-family: 'Poppins', sans-serif;">
        <h1>Halaman Create Barber</h1>
    </center>
    {{-- create table with data from array siswa --}}

    <form method="post" action="/dashboard/barber/add" enctype="multipart/form-data">
        @csrf

        <div class="card">
            <div class="card-body">

                <div class="mb-3">
                    <label for="">Name</label>
                    <input type="text" class="form-control" name="username" id="username" placeholder="Insert Name">
                </div>
                <label for="">Image</label>
                <div id="dropzone" class="dropzone">
                    <div class="drop-message mb-4">
                        Drag and drop files here or click to upload or replace file.
                    </div>
                    <input type="file" class="form-control" name="image" id="file-input" aria-describedby="inputGroupFileAddon04" aria-label="Upload"accept="image/*" onchange="loadFile(event)" style="display:none;">
                    <button type="button" id="browse-btn">Browse</button>
                </div>
                <img id="output"width="100px" height="100px" class="img img-responsive"style='display:none;'/>
                <div class="mb-3">
                    <label for="">Customer</label>
                    <input type="number" class="form-control" name="customer" id="customer" placeholder="Customer">
                </div>
                {{-- <div class="mb-3">
                    <label for="experience" class="form-label">Experience</label>
                    <select class="form-select" name="experience" id="">
                        @foreach ($experience as $class)
                            <option name="experience" value="{{ $class->id }}">{{ $class->name }}</option>
                        @endforeach
                    </select>
                </div> --}}
                <div class="mb-3">
                    <label for="">Experience</label>
                    <input type="text" class="form-control" name="experience" id="experience" placeholder="Input Experience">
                </div>
                <div class="mb-3">
                    <label for="">Rating</label>
                    <input type="text" class="form-control" name="rating" id="rating" placeholder="Input Rating">
                </div>
                <div class="mb-3">
                    <label for="">Range</label>
                    <input type="text" class="form-control" name="range" id="range" placeholder="Input Range">
                </div>
                <div class="mb-3">
                    <label for="">About</label>
                    <input type="text" class="form-control" name="about" id="about" placeholder="Input About">
                </div>

            </div>
            <button type="submit" class="btn btn-primary">Send</button>

        

    </form>
    <script>
        var dropzone = document.getElementById('dropzone');
        var fileInput = document.getElementById('file-input');
        var browseBtn = document.getElementById('browse-btn');
    dropzone.addEventListener('dragenter', (e) => {
            e.preventDefault();
            dropzone.classList.add('dragover');
        });

        dropzone.addEventListener('dragover', (e) => {
            e.preventDefault();
          
        });

        dropzone.addEventListener('dragleave', () => {
            dropzone.classList.remove('dragover');
         
        });
    dropzone.addEventListener('drop', (e) => {
            e.preventDefault();
            dropzone.classList.remove('dragover');
            fileInput.files = e.dataTransfer.files;
            console.log(e.dataTransfer.files);
        });
    browseBtn.addEventListener('click', () => {
            fileInput.click();
        });

    </script>
@endsection

<script>
var loadFile = function(event) {
    var reader = new FileReader();
    reader.onload = function(){
        var output = document.getElementById('output');
       
        output.style.display = 'block'; 
        output.src = reader.result;
      };
      reader.readAsDataURL(event.target.files[0]);
    };
  </script>