@extends('dashboard.layouts.main')

@section('contain')
<div>
    <div class="topdetail">
        <center><h1 >Halaman Edit Barber</h1></center>
    </div>
        <div class="col-lg-8">
            {{-- <div style="padding-bottom: 40px; padding-top: 40px;">
            <a type="button" class="btn btn-warning" href="create">Edit Portofolio</a>
            </div> --}}
            <form method="post" action="/dashboard/barber/update/{{ $barber->id }}" enctype="multipart/form-data">
                @csrf
    
                <div class="mb-3">
                    <label for="username" class="from-label">Name</label>
                    <input type="text" class="form-control" id="username" name="username"
                        value="{{ old('username', $barber->username) }}" disable>
                </div>
    
           
            <label for="">Image</label>
            <div id="dropzone" class="dropzone">
                <div class="drop-message mb-4">
                    Drag and drop files here or click to upload or replace file.
                </div>
                <input type="file" class="form-control" name="image" id="file-input" aria-describedby="inputGroupFileAddon04" aria-label="Upload"accept="image/*" onchange="loadFile(event)" style="display:none;">
                <button type="button" id="browse-btn">Browse</button>
                  </div>
            <img id="output"width="100px" height="100px" class="img img-responsive"style='display:none;'/>
            @if ($barber->image !== null)
            <img id="oldimage" src="/images/barber/{{ $barber->image }}" width="100px" height="100px" class="img img-responsive">
           
            @endif
    
                <div class="mb-3">
                    <label for="password" class="from-label">Customer</label>
                    <input type="number" class="form-control" id="customer" name="customer"
                        value="{{ old('customer', $barber->customer) }}" disable>
                </div>
    
                <div class="mb-3">
                    <label for="password" class="from-label">Experience</label>
                    <input type="text" class="form-control" id="experience" name="experience"
                        value="{{ old('experience', $barber->experience) }}" disable>
                </div>
    
                <div class="mb-3">
                    <label for="password" class="from-label">Rating</label>
                    <input type="text" class="form-control" id="rating" name="rating"
                        value="{{ old('rating', $barber->rating) }}" disable>
                </div>
    
                <div class="mb-3">
                    <label for="password" class="from-label">Range</label>
                    <input type="text" class="form-control" id="range" name="range"
                        value="{{ old('rating', $barber->range) }}" disable>
                </div>
    
                <div class="mb-3">
                    <label for="password" class="from-label">About</label>
                    <input type="text" class="form-control" id="about" name="about"
                        value="{{ old('about', $barber->about) }}" disable>
                </div>
                
                <br>
    
                <button type="submit" class="btn btn-primary">Ubah Data</button>
            </form>
        </div>
</div>


    <script>
        var dropzone = document.getElementById('dropzone');
        var fileInput = document.getElementById('file-input');
        var browseBtn = document.getElementById('browse-btn');
    dropzone.addEventListener('dragenter', (e) => {
            e.preventDefault();
            dropzone.classList.add('dragover');
        });
    
        dropzone.addEventListener('dragover', (e) => {
            e.preventDefault();
          
        });
    
        dropzone.addEventListener('dragleave', () => {
            dropzone.classList.remove('dragover');
         
            });
    dropzone.addEventListener('drop', (e) => {
            e.preventDefault();
            dropzone.classList.remove('dragover');
            fileInput.files = e.dataTransfer.files;
            console.log(e.dataTransfer.files);
            });
    browseBtn.addEventListener('click', () => {
            fileInput.click();
            });
    
    </script>
    @endsection
    
    <script>
        var loadFile = function(event) {
          var reader = new FileReader();
          reader.onload = function(){
            var output = document.getElementById('output');
            var oldimage = document.getElementById('oldimage');
            oldimage.style.display = 'none';
            output.style.display = 'block'; 
            output.src = reader.result;
          };
          reader.readAsDataURL(event.target.files[0]);
        };
      </script>