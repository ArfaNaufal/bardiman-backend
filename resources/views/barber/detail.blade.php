@extends('dashboard.layouts.main')

@section('contain')
<div>
    <div class="topdetail">
    <center><h1 >Halaman Detail Barber</h1></center>
</div>
{{-- create table with data from array siswa --}}

<div class="form-group p-3">
    <label for="">Name</label>
    <input type="text" class="form-control" name="username" id="username" value="{{ $s->username }} " readonly>
</div>

<div class="form-group p-3">
    <label for="" >Image</label><br>
    <img src="/images/barber/{{ $s->image }}" width="100"  class="img img-responsive">
</div>

<div class="form-group p-3">
    <label for="" >Customer</label>
    <input type="text" class="form-control" name="customer" id="customer" value="{{ $s->customer }}" readonly>
</div>

<div class="form-group p-3">
    <label for="" >Experience</label>
    <input type="text" class="form-control" name="experience" id="experience" value="{{ $s->experience }}" readonly>
</div>
 
<div class="form-group p-3">
    <label for="">Rating</label><br>
    @for ($i = 1; $i <= 5; $i++)
    <span class="star {{ $i <= $s->rating ? 'active' : '' }}"></span>
    @endfor
    <input type="text" class="form-control mt-3" name="rating" id="rating" value="{{ $s->rating }} " readonly>
</div>

<div class="form-group p-3">
    <label for="">Range</label>
    <input type="text" class="form-control" name="range" id="range" value="{{ $s->range }} " readonly>
</div>

<div class="form-group p-3">
    <label for="">About</label>
    <input type="text" class="form-control" name="about" id="about" value="{{ $s->about }} " readonly>
</div>

<div class="form-group p-3">
    <div style="display: flex; flex-direction:row; justify-content:space-between; align-items: center; ">
        <label for="">Portofolio</label>
        <div class="form-group p-3">
            <a href="{{url('dashboard/porto/create/'.$s->id)}}" class="btn btn-outline-primary">Add Portofolio</a>
        </div>
    </div>
    
@foreach ( $s->porto as $sdata)
    <div style="display: flex; flex-direction:row; justify-content:space-between; margin-bottom:20px; align-items: center;">
        <img src="/images/porto/{{ $sdata->image }}" width="100"  class="img img-responsive">
        <input type="text" class="form-control" style="height: 50px; margin-right:20px; margin-left: 20px;" name="hour" id="hour" value="{{ $sdata->description }} " readonly>
        <button onclick="location.href=`{{url('dashboard/porto/edit/'.$sdata->id)}}`" class="btn-action" style="height: 50px; background-color: var(--primary-color); color: white;"><i class="uil uil-edit-alt"></i></button>
    </div>
@endforeach

</div>
<div class="form-group p-3">
    <div style="display: flex; flex-direction:row; justify-content:space-between; align-items: center; ">
        <label for="">Hour</label>
        <div class="form-group p-3">
            <a href="{{url('dashboard/schedule/create/'.$s->id)}}" class="btn btn-outline-primary">Add Schedule</a>
        </div>
    </div>
    
@foreach ( $s->schedule as $sdata)
    <div style="display: flex; flex-direction:row; justify-content:space-between; margin-bottom:20px;">
        <input type="text" class="form-control" style="height: 50px; margin-right:20px;" name="hour" id="hour" value="{{ $sdata->hour }} " readonly>
        <button onclick="location.href=`{{url('dashboard/schedule/edit/'.$sdata->id)}}`" class="btn-action" style="height: 50px; background-color: var(--primary-color); color: white;"><i class="uil uil-edit-alt"></i></button>
    </div>
@endforeach

</div>

<div class="form-group p-3">
    <a href="/dashboard/barber/all" class="btn btn-outline-primary">Go Back</a>
</div>
</div>

@endsection