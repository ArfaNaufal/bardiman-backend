<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\promoController;
use App\Http\Controllers\BarberController;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\RecommendationController;

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'menu'], function () {
    Route::get('/all', [MenuController::class, 'index']);
    Route::get('/filter', [MenuController::class, 'filtered']);
});

Route::group(['prefix' => 'Recommendation'], function () {
    Route::get('/all', [RecommendationController::class, 'index']);
});

Route::group(['prefix' => 'barber'], function () {
    Route::get('/all', [BarberController::class, 'index']);
    Route::get('/filter', [BarberController::class, 'disablePickedSchedules']);
    Route::get('/filters', [BarberController::class, 'filtered']);

});

Route::group(['prefix' => 'transaction'], function () {
    Route::get('/all', [TransactionController::class, 'index']);
    Route::post('/add', [TransactionController::class, 'store']);
    Route::delete('/delete/{id}', [TransactionController::class, 'destroy']);
    Route::post('/update/{id}', [TransactionController::class, 'update']);
    Route::get('/filter', [TransactionController::class, 'filtered']); 
});

Route::group(['prefix' => 'promo'], function () {
    Route::post('/add', [promoController::class, 'store']);

});

Route::middleware('auth:sanctum')->group(function (){

   

    Route::get('/user', [AuthController::class, 'getDetails']);
});

   

    Route::group(['prefix' => 'auth'], function () {
        Route::post('/register', [AuthController::class, 'register']);
        Route::post('/login', [AuthController::class, 'login']); 
    });
