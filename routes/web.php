<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MailControler;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\BarberController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\dashboardController;
use App\Http\Controllers\dashboardmenuController;
use App\Http\Controllers\dashboardUserController;
use App\Http\Controllers\dashboardPortoController;
use App\Http\Controllers\dashboardRecapController;
use App\Http\Controllers\dashboardBarberController;
use App\Http\Controllers\dashboardScheduleController;
use App\Http\Controllers\dashboardTransactionController;
use App\Http\Controllers\EmailController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/home', function () {
    return view('home');
});

Route::get('/', [LoginController::class, 'index'])->name('login')->middleware('guest');

Route::get('send-email', [MailControler::class, 'index']);



Route::prefix('dashboard')  ->middleware('auth','isAdmin')->group(function () {

    Route::group(['prefix' => 'menu'], function () {
        Route::get('/all', [dashboardmenuController::class, 'index']);
        Route::get('/detail/{s}', [MenuController::class, 'show']);
        Route::get('/create', [MenuController::class, 'create']);
        Route::post('/add', [MenuController::class, 'store']);
        Route::get('/delete/{id}', [MenuController::class, 'destroy'])->name('delete_menu');
        Route::get('/edit/{id}', [MenuController::class, 'edit']);
        Route::post('/update/{id}', [MenuController::class, 'update']);
        Route::get('/search', [dashboardmenuController::class, 'search'])->middleware('auth')->name('search');
    });

    Route::group(['prefix' => 'barber'], function () {
        Route::get('/all', [dashboardBarberController::class, 'index']);
        Route::get('/create', [dashboardBarberController::class, 'create']);
        Route::post('/add', [dashboardBarberController::class, 'store']);
        Route::get('/detail/{s}', [BarberController::class, 'show']);
        Route::get('/delete/{barber}', [dashboardBarberController::class, 'destroy']);
        Route::get('/edit/{barber}', [BarberController::class, 'edit']);
        Route::post('/update/{barber}', [BarberController::class, 'update']);
        Route::get('/search', [dashboardBarberController::class, 'search'])->middleware('auth')->name('search');
    });

    Route::group(['prefix' => 'transaction'], function () {
        Route::get('/all', [dashboardTransactionController::class, 'index']);
        Route::get('/success/{id}', [dashboardTransactionController::class, 'success']);
        Route::get('/search', [dashboardBarberController::class, 'search'])->middleware('auth')->name('search');
    });

    // Route::group(['prefix' => 'transaction'], function () {
    //     Route::get('/all', [dashboardTransactionController::class, 'index']);
    //     Route::get('/detail/{s}', [dashboardTransactionController::class, 'show']);

    // });

    Route::group(['prefix' => 'recap'], function () {
        Route::get('/all', [dashboardRecapController::class, 'index']);
        Route::get('/excel/{data}', [dashboardRecapController::class, 'excel']);

    });

    // Route::group(['prefix' => 'user'], function () {
    // Route::get('/all', [dashboardUserController::class, 'index']);
    // Route::get('/detail/{s}', [dashboardUserController::class, 'show']);

    // });

    Route::group(['prefix' => 'schedule'], function () {
    Route::get('/all', [dashboardScheduleController::class, 'index']);
    Route::get('/create/{barberid}', [dashboardScheduleController::class, 'create']);
    Route::post('/add/{barberid}', [dashboardScheduleController::class, 'store']);
    Route::get('/detail/{s}', [dashboardScheduleController::class, 'show']);
    Route::get('/delete/{schedule}', [dashboardScheduleController::class, 'destroy']);
    Route::get('/edit/{schedule}', [dashboardScheduleController::class, 'edit']);
    Route::post('/update/{schedule}', [dashboardScheduleController::class, 'update']);
    Route::get('/search', [dashboardScheduleController::class, 'search'])->middleware('auth')->name('search');

    });

    Route::group(['prefix' => 'porto'], function () {
    Route::get('/all', [dashboardPortoController::class, 'index']);
    Route::get('/create/{barberid}', [dashboardPortoController::class, 'create']);
    Route::post('/add/{barberid}', [dashboardPortoController::class, 'store']);
    Route::get('/detail/{s}', [dashboardPortoController::class, 'show']);
    Route::get('/delete/{porto}', [dashboardPortoController::class, 'destroy']);
    Route::get('/edit/{porto}', [dashboardPortoController::class, 'edit']);
    Route::post('/update/{porto}', [dashboardPortoController::class, 'update']);
    Route::get('/search', [dashboardPortoController::class, 'search'])->middleware('auth')->name('search');

    });

});

Route::group(['prefix' => 'session'], function () {
    Route::get('/login', [LoginController::class, 'index'])->name('login')->middleware('guest');
    Route::post('/logins', [LoginController::class, 'auth']);
    Route::get('/logout', [LoginController::class, 'logout']);


});

Route::get('/forgot-password', function () {
    return view('auth.forgot-password');
})->middleware('guest')->name('password.request');

Route::post('/forgot-password', [EmailController::class, 'email'])->middleware('guest')->name('password.email');

Route::get('/reset-password/{token}', function (string $token) {
    return view('auth.reset-password', ['token' => $token]);
})->middleware('guest')->name('password.reset');

Route::post('/reset-password', [EmailController::class, 'update'])->middleware('guest')->name('password.update');

