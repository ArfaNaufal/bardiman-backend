<?php

namespace App\Http\Controllers;

use App\Models\Barber;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;


class dashboardTransactionController extends Controller
{
    public function index(Request $request)
{   

    $currentDate = Carbon::now()->toDateString();
    $data_transaction = Transaction::filter(request(['barber_id','create_atreq']))->where('status','success')->with(['barber','customer','menu','schedule'])->paginate(5);

    $query='';
        if ($request->queryvalue != null){
            $query=$request->queryvalue;
            $data_transaction=Transaction::where('status','success')->where('transac_code','like','%'.$query.'%')->Paginate(5);

        }


    return view('dashboard.history', [
        "data_transaction" => $data_transaction,
        "data_barber" => Barber::all(),
        "data_transactiond" => Transaction::where('status','success'),
       
    ]);
}

    function success($id)   {
        $data=Transaction::where("id",$id)->first();
        $data->update([
            "status" => "done",
        ]);
        return back();
    }


}
