<?php

namespace App\Http\Controllers;

use App\Models\Barber;
use App\Models\Schedule;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class BarberController extends Controller
{
    public function index()
    {
        $barber = Barber::all();
        return response()->json(
            [
                'barbers' => $barber
            ]
        );
    }

    public function filtered(Request $request)
    {
        $date = $request->input('date');
        $barberId = $request->input('barber_id');

        if (!$date) {
            return response()->json(['error' => 'Date parameter is missing.'], 400);
        }

        $query = Transaction::where('date', $date);

        if ($barberId) {
            $query->where('barber_id', $barberId);
        }

        $count = $query->get();

        return response()->json(['count' => $count]);



        // $category_id = 1;
        // // $menu = Menu::where('category_id', $category_id)->get();
        // $schedule = Schedule::filter(request(['barber_id']))->get();

        // return response()->json(['filtered' => $schedule]);
    }

    
// Function to disable the picked schedule IDs
    function disablePickedSchedules(Request $request)
    {
        $date = $request->input('date');
        $barberId = $request->input('barber_id');

        

        $query = Transaction::where('date', $date);

        if ($barberId) {
            $query->where('barber_id', $barberId);
        }

        $count = $query->get();

        $pickedTransactions = $count->pluck('schedule_id')->toArray();
        $allScheduleIds = Schedule::all()->pluck('id')->toArray();

        // Filter out the picked schedule IDs
        $availableScheduleIds = array_diff($allScheduleIds, $pickedTransactions);

        // Use whereIn instead of where
        $finalquery = Schedule::whereIn('id', $availableScheduleIds);
        $finalquery->where('barber_id', $barberId);

        $final = $finalquery->get();

        return response()->json(['availableschedule' => $final]);
    }

    public function show(Barber $s){
        return view('barber.detail',[
         "s" => $s
        ]);
     }

    public function destroy($id)
    {
        $barberdl = DB::table('barbers')->where('id', $id)->first();
        $old_image = public_path('images/barber/') . $barberdl->image;
        if (file_exists($old_image)) {
            unlink($old_image);
        }
        DB::table('barbers')->where('id', $id)->delete();
        return redirect('dashboard/barber/all')->with('success', 'Data Berhasil Dihapus!');


        // Barber::destroy($barber->id );
        // return redirect('dashboard/barber/all') ->with('success', 'Data Berhasil Dihapus!');
    }

    public function edit(Barber $barber)
    {
        //return view
        return view('barber.edit', ['barber' => $barber]);
    }

    public function update(Request $request, $id)
    {
        //validate request
        $validateData = Validator::make($request->all(), [
            'username'      => 'required|max:255',
            'image'         => 'nullable',
            'customer'      => 'required',
            'experience'    => 'required',
            'rating'        => 'required',
            'range'         => 'required',
            'about'         => 'required'
        ]);
        // dd($validateData);
        if ($validateData->fails()) {
            return redirect('/dashboard/barber/all')->with('failed', $validateData->errors());

        }
        $barberpd = DB::table('barbers')->where('id', $id)->first();
        if ($request->hasFile('image')) {
            $old_image = public_path('images/barber/') . $barberpd->image;
            if (file_exists($old_image)) {
                unlink($old_image);
            }
            $image = $request->file('image');
            $image_name = time() . '.' . $image->extension();
            $image->move(public_path('images/barber'), $image_name);
            DB::table('barbers')->where('id', $id)->update([
                'username'      => $request->username,
                'image'         => $image_name,
                'customer'      => $request->customer,
                'experience'    => $request->experience,
                'rating'        => $request->rating,
                'range'         => $request->range,
                'about'         => $request->about,
            ]);
        } else {
            DB::table('barbers')->where('id', $id)->update([
                'username'      => $request->username,
                'image'         => $barberpd->image,
                'customer'      => $request->customer,
                'experience'    => $request->experience,
                'rating'        => $request->rating,
                'range'         => $request->range,
                'about'         => $request->about,
            ]);
        }


        return redirect('/dashboard/barber/all')->with('success', 'Data Berhasil Diedit!');
    }
}
