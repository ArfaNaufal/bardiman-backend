<?php

namespace App\Http\Controllers;

use App\Models\Schedule;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class TransactionController extends Controller
{
    public function index()
    {
        $transaction = Transaction::all();
        return response()->json([
            'transaction' => $transaction,
        ], 200);
        
    }

    public function filtered()
    {
        

        // $barber_id = DB::table('transactions')
        //             ->select(DB::raw('barber_id'))
        //             ->get();

        // $user_id = DB::table('transactions')
        //             ->select(DB::raw('user_id'))
        //             ->get();
        // $users = Transaction::filter(request(['user_id']))
        // ->select(DB::raw('username from users where id = user_id'))
        // ->get();

        // users = DB::table('users')
        //  ->select(DB::raw('username from users'))
        //  ->where('id', '=', DB::table('transaction')('user_id' ))
        //  ->get();
        // $barber = Transaction::filter(request(['user_id']))
        //             ->join('barbers', 'barbers.id', '=', 'transactions.barber_id')
        //             ->select('barbers.username')
        //             ->get();
        // $users = Transaction::filter(request(['user_id']))
        //             ->join('users', 'users.id', '=', 'transactions.user_id')
        //             ->select('users.username')
        //             ->get();
        // $menu = Transaction::filter(request(['user_id']))
        //             ->join('menus', 'menus.id', '=', 'transactions.menu_id')
        //             ->select('menus.name')
        //             ->get();
        // $schedule = Transaction::filter(request(['user_id']))
        //             ->join('schedules', 'schedules.id', '=', 'transactions.schedule_id')
        //             ->select('schedules.hour')
        //             ->get();
        
        //             ->orderBy('id', 'desc')->get();
        // $transaction = Transaction::filter(request(['user_id']))
        $transaction = DB::table('transactions')
    ->join('barbers', 'barber_id', '=', 'barbers.id')
    ->join('users', 'user_id', '=', 'users.id')
    ->join('menus', 'menu_id', '=', 'menus.id')
    ->join('schedules', 'schedule_id', '=', 'schedules.id')
    ->select('transactions.*', 'barbers.username as username-barber', 'users.username as username-user', 'menus.name as menu-name', 'schedules.hour as schedule-hour')
    ->orderBy('transactions.id', 'desc')
    ->distinct()
    ->get();


        return response()->json([
            'transaction' => $transaction
    ]);
    }

    public function generateRandomCode($length = 10)
    {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $randomCode = '';
        for ($i = 0; $i < $length; $i++) {
            $randomCode .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomCode;
    }

    public function store(Request $request)
    {
        $maxAttempts = 100; // Maximum attempts to generate a unique code
        $attempts = 0;
        $randomCode = $this->generateRandomCode();
        while (Transaction::where('transac_code', $randomCode)->exists() && $attempts < $maxAttempts) {
            $randomCode = $this->generateRandomCode();
            $attempts++;
        }

        // If unable to generate unique code after max attempts, handle the error as needed
        if ($attempts >= $maxAttempts) {
            return response()->json(['error' => 'Unable to generate unique random code. Please try again.'], 500);
        }

        $currentDate = Carbon::now();

        DB::table('transactions')->insert([
            'transac_code'         => $randomCode,
            'barber_id'            => $request->barber_id,
            'user_id'              => $request->user_id,
            'menu_id'              => $request->menu_id,
            'schedule_id'          => $request->schedule_id,
            'recommendation_id'    => $request->recommendation_id,
            'date'                 => $request->date,
            'price'                => $request->price,
            'status'               => $request->status
        ]);

        // You can return a response or redirect as needed
        return response()->json(['message' => 'Data inserted successfully']);

        // $validateData = Validator::Make($request->all(), [
        //     'barber_id'     => 'required',
        //     'user_id'       => 'required',
        //     'menu_id'       => 'required',
        //     'schedule_id'   => 'required',
        //     'date'          => 'required',
        //     'price'         => 'required',
        //     'status'        => 'required'

        // ]);
        // if ($validateData->fails()) {
        //     return response()->json([
        //         'message' => 'Validation Error',
        //         'data' => $validateData->errors()
        //     ], 400);
        // }
        // else{
        //     $transaction = Transaction::create($request->all());
        //     return response()->json([
        //         'message' => 'Transaction Created',
        //         'data' => $transaction
        //     ], 200);
        // }
        
    }

    public function destroy($id)
    {
        Transaction::findOrFail($id)->delete();
        return response()->json([
            'message' => 'Transaction Deleted',
        ], 204);
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'promo_status' => 'required|integer',
        ]);

        $model = Transaction::findOrFail($id);

        $model->update($validatedData);

        return response()->json(['message' => 'Model updated successfully', 'data' => $model]);
    }

    public function handleXenditCallback(Request $request)
    {
        // Ini akan menjadi Token Verifikasi Callback Anda yang dapat Anda peroleh dari dasbor.
        // Simpan token ini di file .env atau sesuaikan dengan cara penyimpanan konfigurasi yang lebih aman.
        $xenditXCallbackToken = 'RIdgNqgv2Yx0px37d1wdgvcxOYuD1K75SgzG8HxnCuejtdp7';

        // Mendapatkan token callback dari header permintaan
        $xIncomingCallbackTokenHeader = $request->header('x-callback-token');

        // Memastikan permintaan datang dari Xendit dengan membandingkan token yang masuk dengan token verifikasi callback Anda
        if ($xIncomingCallbackTokenHeader === $xenditXCallbackToken) {
            // Permintaan masuk diverifikasi berasal dari Xendit

            // Mendapatkan semua input pesan dalam format JSON teks mentah
            $rawRequestInput = $request->getContent();
            // Mengubah input mentah menjadi array asosiatif
            $arrRequestInput = json_decode($rawRequestInput, true);

            // Lakukan proses penyimpanan data ke database berdasarkan informasi callback
            // Contoh: Menyimpan data ke tabel pembayaran
            
            $paymentData = [
                'xendit_id' => $arrRequestInput['id'],
                'external_id' => $arrRequestInput['external_id'],
                'user_id' => $arrRequestInput['user_id'],
                'status' => $arrRequestInput['status'],
                'paid_amount' => $arrRequestInput['paid_amount'],
                'paid_at' => $arrRequestInput['paid_at'],
                'payment_channel' => $arrRequestInput['payment_channel'],
                'payment_destination' => $arrRequestInput['payment_destination'],
            ];

            // Simpan data ke database (gunakan model atau cara penyimpanan data lainnya)
            // Misalnya:
            // \App\Models\Payment::create($paymentData);

            // Anda dapat melakukan pengecekan atau aktivasi tertentu di aplikasi atau sistem Anda berdasarkan data callback.

            // Berikan respons sukses jika berhasil memproses callback
            return response()->json(['status' => 'success'], 200);
        } else {
            // Permintaan bukan dari Xendit, tolak dan buang pesan dengan HTTP status 403
            return response()->json(['status' => 'error', 'message' => 'Forbidden'], 403);
        }
    }



}
