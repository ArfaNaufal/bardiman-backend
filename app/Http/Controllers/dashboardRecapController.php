<?php

namespace App\Http\Controllers;

use App\Exports\TransactionExport;
use App\Models\Barber;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Maatwebsite\Excel\Facades\Excel;



class dashboardRecapController extends Controller
{
    public function index(Request $request)
{   

    $currentDate = Carbon::now()->toDateString();

    
    $transactionsToday = Transaction::whereDate('created_at', $currentDate)->where('status','done')->get();

    if(request('barber_id')!=null){
        $transactionsToday = Transaction::where('barber_id',request('barber_id'))->where('status','done')->get();
    }

    if(request('create_atreq')!=null){
        $transactionsToday = Transaction::where('tanggal',request('create_atreq'))->where('status','done')->get();
    }

    if(request('create_atreq')!=null&&request('barber_id')!=null){
        $transactionsToday = Transaction::where('barber_id',request('barber_id'))->where('tanggal',request('create_atreq'))->where('status','done')->get();
    }

    if(request('create_atreq')==null&&request('barber_id')==null){
        $transactionsToday = Transaction::where('status','done');
    }

    

    


    $data_transaction = Transaction::filter(request(['barber_id','create_atreq']))->where('status','done')->with(['barber','customer','menu','schedule'])->paginate(5);
    $data_transaction2 = Transaction::filter(request(['barber_id','create_atreq']))->where('status','done')->with(['barber','customer','menu','schedule'])->get();

    $idbarber = '';
    if(request('barber_id')!=null){
        $idbarber = request('barber_id');
    }

    $created_atd = '';
    if(request('create_atreq')!=null){
        $created_atd = request('create_atreq');
    }

    $query='';
        if ($request->queryvalue != null){
            $query=$request->queryvalue;
            $data_transaction=Transaction::where('status','done')->where('transac_code','like','%'.$query.'%')->paginate(5);
            $data_transaction2=Transaction::where('status','done')->where('transac_code','like','%'.$query.'%')->get();
            $transactionsToday = Transaction::where('transac_code','like','%'.$query.'%')->where('status','done')->get();
            // return $transactionsToday;
        }
        
    $total = $transactionsToday->sum('price');
    return view('dashboard.recap', [
        "data_transaction" => $data_transaction,
        "data_transaction2" => $data_transaction2,
        "value_search" => $request->queryvalue,
        "data_barber" => Barber::all(),
        "data_transactiond" => Transaction::distinct('tanggal')->pluck('tanggal')->where('status','done'),
        'total' => $total,
        'idbarber' => $idbarber,
        'barbervalue'=>Barber::where('id',$idbarber)->first(),
        'created_atd' => $created_atd

        
    ]);
    
}

    public function excel(Request $request){
        
    $data_transaction = Transaction::filter(request(['barber_id','create_atreq']))->where('status','done')->with(['barber','customer','menu','schedule'])->get();
   
        $total = $data_transaction->sum('price');

        $exportData = [
        'done' => $data_transaction,  
        'total' => $total, 
        
        ];
         return Excel::download(new TransactionExport($exportData),'Laporan' . '.xlsx');
        }
}
