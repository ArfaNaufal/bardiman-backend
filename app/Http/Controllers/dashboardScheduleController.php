<?php

namespace App\Http\Controllers;

use App\Models\Barber;
use App\Models\Schedule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class dashboardScheduleController extends Controller
{
    public function index(){
        return view('dashboard.schedule',["data_schedule" => Barber::with("schedule")->Paginate(5)]);
    }
    public function create($barberid)
    {
        return view('schedule.create', [

            'barberid' => $barberid,
            'schedule' => Schedule::all()
        ]);
    }
    public function store(Request $request,$barberid)
    {
        $validateData = Validator::make($request->all(), [
            'hour'   => 'required|max:255',
        ]);
        
        if ($validateData->fails()) {
            return redirect('/dashboard/barber/detail'.$barberid)->with('failed', $validateData->errors());
        }
       
        $checkempty = Schedule::where('barber_id', $barberid)->where('hour',$request->hour)->get();
        if ($checkempty->isEmpty()){
    
            $menu = DB::table('schedules')->insert([
                'barber_id'   => $barberid,
                'hour'   => $request->hour,
                
    
            ]);
    
    
            return redirect('/dashboard/barber/detail/'.$barberid)->with('success', 'Data Berhasil Ditambahkan!');
        }
        else {
            return redirect('/dashboard/barber/detail/'.$barberid)->with('success', 'Data Gagal Ditambahkan!');
           
           
        }
    }
    public function edit(Schedule $schedule)
    {
        //return view
        return view('schedule.edit', ['schedule' => $schedule]);
    }
    public function update(Request $request, $id)
    {
        //validate request
        $validateData = Validator::make($request->all(), [
           
            'hour'   => 'required|max:255',
        ]);
        // dd($validateData);
        if ($validateData->fails()) {
            return redirect('/dashboard/barber/all')->with('failed', $validateData->errors());

        }
        $schedulepd = DB::table('schedules')->where('id', $id)->first();
        if ($request->hasFile('image')) {
            $old_image = public_path('images/schedule/') . $schedulepd->image;
            if (file_exists($old_image)) {
                unlink($old_image);
            }
            $image = $request->file('image');
            $image_name = time() . '.' . $image->extension();
            $image->move(public_path('images/schedule'), $image_name);
            DB::table('schedules')->where('id', $id)->update([
                
                'hour'   => $request->hour,
            ]);
        } else {
            DB::table('schedules')->where('id', $id)->update([
                'hour'   => $request->hour,
            ]);
        }


        return redirect('/dashboard/barber/detail/'.$schedulepd->barber_id)->with('success', 'Data Berhasil Diedit!');
    }
    public function show($idbarber ){
        return view('schedule.detail',[
         "s" => Barber::where('id', $idbarber)->with("schedule")->first(),
        ]);
     }

     public function destroy($id)
     {
         DB::table('schedules')->where('id', $id)->delete();
         return redirect('dashboard/schedule/all')->with('success', 'Data Berhasil Dihapus!');
 
     }

}
