<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use App\Models\Category;
use Illuminate\Http\Request;

class dashboardmenuController extends Controller
{
    public function index(Request $request){
        $order=Menu::Paginate(5);
        $query='';
        if ($request->queryvalue != null){
            $query=$request->queryvalue;
            $order=Menu::where('name','like','%'.$query.'%')->Paginate(5);

        }
        return view('dashboard.menu',["data_menu" => $order, "category" => Category::all(),'searchvalue'=>$query]);

    }
}
