<?php

namespace App\Http\Controllers;

use App\Models\promo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class promoController extends Controller
{
    public function generateRandomCode($minLength = 8, $maxLength = 10)
    {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        $randomCode = 'PRM-';
        $length = rand($minLength, $maxLength);
        for ($i = 0; $i < $length; $i++) {
            $randomCode .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomCode;
    }

    public function store(Request $request)
    {

        
        
        // Generate a unique random code
        $maxAttempts = 100; // Maximum attempts to generate a unique code
        $attempts = 0;
        $randomCode = $this->generateRandomCode();
        while (promo::where('promo_code', $randomCode)->exists() && $attempts < $maxAttempts) {
            $randomCode = $this->generateRandomCode();
            $attempts++;
        }

        // If unable to generate unique code after max attempts, handle the error as needed
        if ($attempts >= $maxAttempts) {
            return response()->json(['error' => 'Unable to generate unique random code. Please try again.'], 500);
        }

        // Insert data with the generated unique random code
       
        DB::table('promos')->insert([
            'user_id'       => $request->user_id,
            'promo_value'   => $request->promo_value,
            'promo_code'    => $randomCode
            
        ]);

        // You can return a response or redirect as needed
        return response()->json(['message' => 'Data inserted successfully']);
    }
}
