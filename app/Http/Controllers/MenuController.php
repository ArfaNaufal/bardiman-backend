<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class MenuController extends Controller
{
    public function index()
    {
        $menu = Menu::all();
        // $menu = DB::table('menus')->get();
        $category = Category::all();
        return response()->json(
            [
                'menus' => $menu,
                'category' => $category
            ]

        );
    }

    public function filtered()
    {
        $category_id = 1;
        // $menu = Menu::where('category_id', $category_id)->get();
        $menu = Menu::filter(request(['category_id']))->get();

        return response()->json(['filtered' => $menu]);
    }

    public function show(Menu $s)
    {
        return view('menu.detail', [
            "s" => $s
        ]);
    }

    public function create()
    {
        return view('menu.create', [
            'category' => Category::all()
        ]);
    }

    public function store(Request $request)
    {
        $validateData = Validator::make($request->all(), [
            'category_id'   => 'required',
            'name'          => 'required|max:255',
            'description'   => 'required|max:255',
            'duration'      => 'required',
            'image'         => 'required|mimes:jpeg,png',
            'Price'         => 'required'

        ]);
        if ($validateData->fails()) {
            return redirect('/dashboard/menu/all')->with('failed', $validateData->errors());
        }
        $image = $request->file('image');
        // dd($image);
        $image_name = time() . '.' . $image->extension();
        $image->move(public_path('images/menu'), $image_name);

        $menu = DB::table('menus')->insert([
            'category_id'   => $request->category_id,
            'name'          => $request->name,
            'description'   => $request->description,
            'duration'      => $request->duration,
            'image'         => $image_name,
            'Price'         => $request->Price

        ]);


        return redirect('/dashboard/menu/all')->with('success', 'Data Berhasil Ditambahkan!');
    }

    public function destroy($id)
    {

        $menudl = DB::table('menus')->where('id', $id)->first();
        $old_image = public_path('images/menu/') . $menudl->image;
        if (file_exists($old_image)) {
            unlink($old_image);
        }
        DB::table('menus')->where('id', $id)->delete();
        return redirect('dashboard/menu/all')->with('success', 'Data Berhasil Dihapus!');

        // Menu::destroy($menu->id);
        
    }

    public function edit($id)
    {
        $menu=menu::find($id);
        //return view
        return view('menu.edit', ['menu' => $menu, 'category' => Category::all()]);
    }

    public function update(Request $request, $id)
    {
        $validateData = Validator::make($request->all(), [
            'category_id'   => 'required',
            'name'          => 'required|max:255',
            'description'   => 'required|max:255',
            'duration'      => 'required',
            'image'         => 'nullable',
            'Price'         => 'required'
        ]);
        // dd($validateData);
        if ($validateData->fails()) {
            return redirect('/dashboard/menu/all')->with('failed', $validateData->errors());

        }
        $menupd = DB::table('menus')->where('id', $id)->first();
        if ($request->hasFile('image')) {
            $old_image = public_path('images/menu/') . $menupd->image;
            if (file_exists($old_image)) {
                unlink($old_image);
            }
            $image = $request->file('image');
            $image_name = time() . '.' . $image->extension();
            $image->move(public_path('images/menu'), $image_name);
            DB::table('menus')->where('id', $id)->update([
                'category_id'   => $request->category_id,
                'name'          => $request->name,
                'description'   => $request->description,
                'duration'      => $request->duration,
                'image'         => $image_name,
                'Price'         => $request->Price
            ]);
        } else {
            DB::table('menus')->where('id', $id)->update([
                'category_id'   => $request->category_id,
                'name'          => $request->name,
                'description'   => $request->description,
                'duration'      => $request->duration,
                'image'         => $menupd->image,
                'Price'         => $request->Price
            ]);
        }


        return redirect('/dashboard/menu/all')->with('success', 'Data Berhasil Diedit!');
    }
}
