<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Recommendation;

class RecommendationController extends Controller
{
    public function index()
    {
        $recommendation = Recommendation::all();
        return response()->json([
            'recommendation' => $recommendation,
        ], 200);
        
    }
}
