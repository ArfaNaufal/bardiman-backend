<?php

namespace App\Http\Controllers;

use App\Models\porto;
use App\Models\Barber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class dashboardBarberController extends Controller
{
    public function index(Request $request){
        $order=Barber::Paginate(5);
        $query='';
        if ($request->queryvalue != null){
            $query=$request->queryvalue;
            $order=Barber::where('username','like','%'.$query.'%')->with('porto')->Paginate(5);

        }
        // session(['last_index' => $order->lastItem()-1]);
        return view('dashboard.barber',["data_barber" => $order,'searchvalue'=>$query]);
    }
    public function create()
    {
        return view('barber.create', [
            'barber' => Barber::all()
        ]);
    }
    public function store(Request $request)
    {
        $validateData = Validator::make($request->all(), [
            'id'   => 'required',
            'username'      => 'required|max:255',
            'image'         => 'required|mimes:jpeg,png',
            'customer'      => 'required',
            'experiece'   => 'required',
            'rating'   => 'required',
            'range'   => 'required',
            'about'   => 'required',

        ]);
        // if ($validateData->fails()) {
        //     return redirect('/dashboard/barber/all')->with('failed', $validateData->errors());
        // }
        
        $image = $request->file('image');
        // dd($image);
        $image_name = time() . '.' . $image->extension();
        $image->move(public_path('images/barber'), $image_name);

        $menu = DB::table('barbers')->insert([
            'username'          => $request->username,
            'image'         => $image_name,
            'customer'   => $request->customer,
            'experience'      => $request->experience,
            'rating'         => $request->rating,
            'range'         => $request->range,
            'about'         => $request->about,

        ]);


        return redirect('/dashboard/barber/all')->with('success', 'Data Berhasil Ditambahkan!');
    }
    public function destroy($id)
    {
        $barberdl = DB::table('barbers')->where('id', $id)->first();
        $old_image = public_path('images/barber/') . $barberdl->image;
        if (file_exists($old_image)) {
            unlink($old_image);
        }
        $porto = porto::where('barber_id',$id)->delete();
        DB::table('barbers')->where('id', $id)->delete();
        return redirect('dashboard/barber/all')->with('success', 'Data Berhasil Dihapus!');


        // Barber::destroy($barber->id );
        // return redirect('dashboard/barber/all') ->with('success', 'Data Berhasil Dihapus!');
    }

}
