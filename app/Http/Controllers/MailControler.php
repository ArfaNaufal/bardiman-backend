<?php

namespace App\Http\Controllers;

use App\Mail\BardimanMail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class MailControler extends Controller
{
    public function index()
    {
        $mailData = [
            'title' => 'Mail from Bardiman',
            'body' => 'This is for testing email using smtp'
        ];

        Mail::to('vincenciogfs15@gmail.com')->send(new BardimanMail($mailData));

        // dd('Email send successfully');
    }
}