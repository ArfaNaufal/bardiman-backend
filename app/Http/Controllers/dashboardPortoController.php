<?php

namespace App\Http\Controllers;

use App\Models\Barber;
use Illuminate\Http\Request;
use App\Models\porto;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class dashboardPortoController extends Controller
{
    public function index(){
        return view('dashboard.porto',["data_porto" => porto::Paginate(5)]);
    }
    public function create($barberid)
    {
        return view('porto.create', [
            'porto' => porto::all(),
            'barberid' => $barberid
        ]);
    }
    public function filtered()
    {
        $category_id = 1;
        // $menu = Menu::where('category_id', $category_id)->get();
        $porto = porto::filter(request(['barber_id']))->get();

        return response()->json(['filtered' => $porto]);
    }

    public function show(porto $s){
        return view('porto.detail',[
         "s" => $s
        ]);
     }

    public function destroy($id)
    {
        $portodl = DB::table('portos')->where('id', $id)->first();
        $old_image = public_path('images/porto/') . $portodl->image;
        if (file_exists($old_image)) {
            unlink($old_image);
        }
        DB::table('portos')->where('id', $id)->delete();
        return redirect('dashboard/porto/all')->with('success', 'Data Berhasil Dihapus!');

    }

    public function edit(porto $porto)
    {
        //return view
        return view('porto.edit', ['porto' => $porto]);
    }

    public function update(Request $request, $id)
    {
        //validate request
        $validateData = Validator::make($request->all(), [
           
            'description'      => 'required',
            'image'         => 'nullable',
            
        ]);
        // dd($validateData);
        if ($validateData->fails()) {
            return redirect('/dashboard/porto/all')->with('failed', $validateData->errors());

        }
        $portopd = DB::table('portos')->where('id', $id)->first();
        if ($request->hasFile('image')) {
            $old_image = public_path('images/porto/') . $portopd->image;
            if (file_exists($old_image)) {
                unlink($old_image);
            }
            $image = $request->file('image');
            $image_name = time() . '.' . $image->extension();
            $image->move(public_path('images/porto'), $image_name);
            DB::table('portos')->where('id', $id)->update([
                'description'      => $request->description,
                'image'         => $image_name,
            ]);
        } else {
            DB::table('portos')->where('id', $id)->update([
                'description'      => $request->description,
            ]);
        }


        return redirect('/dashboard/barber/detail/'.$portopd->barber_id)->with('success', 'Data Berhasil Diedit!');
    }
    public function store(Request $request,$barberid)
    {
        $validateData = Validator::make($request->all(), [
            'description'   => 'required|max:255',
            'image'         => 'required|mimes:jpeg,png',

        ]);
        
        if ($validateData->fails()) {
            return redirect('/dashboard/barber/detail'.$barberid)->with('failed', $validateData->errors());
        }
        $image = $request->file('image');
        $image_name = time() . '.' . $image->extension();
        $image->move(public_path('images/porto'), $image_name);
        $checkempty = porto::where('barber_id', $barberid)->where('description',$request->description)->get();
        if ($checkempty->isEmpty()){
    
            $menu = DB::table('portos')->insert([
                'barber_id'   => $barberid,
                'description'   => $request->description,
                'image'         => $image_name,
    
            ]);
    
    
            return redirect('/dashboard/barber/detail/'.$barberid)->with('success', 'Data Berhasil Ditambahkan!');
        }
        else {
            return redirect('/dashboard/barber/detail'.$barberid)->with('success', 'Data Gagal Ditambahkan!');
           
           
        }
    }
}
