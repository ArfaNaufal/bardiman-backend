<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'username'           => 'required',
            'password'           => 'required',
            'profile_photo_path' => 'nullable',
            'birthdate'          => 'nullable',
            'gender'             => 'nullable',
            'adress'             => 'nullable',
            'phone_number'       => 'nullable',
        ]);

        $model = User::findOrFail($id);

        $model->update($validatedData);

        return response()->json(['message' => 'Model updated successfully', 'data' => $model]);
    }

    public function destroy($id)
    {
        User::findOrFail($id)->delete();
        return response()->json([
            'message' => 'Transaction Deleted',
        ], 204);
    }
}
