<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index()
    {
        return view('session.login', ["data_user" => User::all()]);
    }

    public function create()
    {
        return view('session.create', ["user" => User::all()]);
    }
    
    public function auth(Request $request)
    {
        //validate data
        $credentials = $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        // if (auth()->attempt($credentials)) {
        //     $request->session()->regenerate();
        //     return redirect()->intended('/dashboard/menu/all');
        //     # code...
        // }
        
        $credentials = $request->only('email', 'password');
        if(Auth::attempt($credentials)){
            $request->session()->regenerate();
            return redirect()->intended('/dashboard/menu/all'); //ga pake succes karna auto home
        }

        return redirect(route('login'))->with([
            'loginError' => 'LOGIN GAGAL YA GES!',
        ]);
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/session/login');
    }
}
