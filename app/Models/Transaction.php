<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function scopeFilter($query, array $filters)
    {

        if (isset($filters['user_id'])) {
            $query->where('user_id', $filters['user_id']);
        }

        if (isset($filters['barber_id'])) {
            $query->where('barber_id', $filters['barber_id']);
        }

        if (isset($filters['create_atreq'])) {
            $query->where('tanggal', $filters['create_atreq']);
        }
    }
    public function barber(){
        return $this->belongsTo(Barber::class,'barber_id','id');
    }

    public function customer(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function menu(){
        return $this->belongsTo(Menu::class,'menu_id','id');
    }

    public function schedule(){
        return $this->belongsTo(Schedule::class,'schedule_id','id');
    }

    // public static function getDataByDataRange($startDate, $endDate)
    // {
    //     return self::whereBetween('tanggal',[$startDate,$endDate])->get();
    // }
    
}
