<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    use HasFactory;

    public function barber()
    {
        return $this->belongsTo(Barber::class);
    }

    public function scopeFilter($query, array $filters)
    {

        if (isset($filters['barber_id'])) {
            $query->where('barber_id', $filters['barber_id']);
        }
        
    }
}
