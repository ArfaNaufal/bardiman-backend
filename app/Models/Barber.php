<?php

namespace App\Models;

use App\Models\porto;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Barber extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function porto()
    {
        return $this->hasMany(porto::class);
    }

    public function schedule()
    {
        return $this->hasMany(Schedule::class);
    }
}
