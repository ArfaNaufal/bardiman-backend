<?php

namespace App\Models;

use App\Models\Barber;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class porto extends Model
{
    use HasFactory;

    public function barber()
    {
        return $this->belongsTo(Barber::class);
    }
}
