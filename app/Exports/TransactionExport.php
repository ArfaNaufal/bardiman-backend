<?php

namespace App\Exports;

use App\Models\Transaction;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithMapping;

class TransactionExport implements FromView, WithColumnWidths, WithMapping, WithColumnFormatting
{
    
    protected $data;

    public function __construct(array $data)
    {
        
        $this->data = $data;
    }

    public function view(): View
    {
        return view('dashboard.excel', $this->data);
    }
    public function columnWidths(): array 
    {
        return [
            'A' => 15,
            'B' => 20,
            'C' => 15,
            'D' => 15,
            'E' => 15,
            'F' => 15,
            'G' => 15,
            'H' => 15,
        ];
    }

    public function columnFormats(): array {
        return [
            'H' => 'Rp#,##0.00_-',
        ];
    }

    public function map($done): array
    {
        return [
            $done->transac_code
        ];
    }

    
    public function collection()
    {
        return Transaction::whereBetween('tanggal',[request('start_date'), request('end_date')])->get();
    }
}

